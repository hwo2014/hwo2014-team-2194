import json
import socket
import sys
from pprint import pprint

class Track(object):

	def __init__(self, data):
		self.name = data["name"]
		self.id = data["id"]
		self.pieces = data["pieces"]

	def distanceToThisPiece(pieceIndex):
		for piece in self.track[0:pieceIndex]:
			totalDist += piece["length"]
		return totalDist


class NoobBot(object):

	def __init__(self, socket, name, key):
		self.myCar = None
		self.socket = socket
		self.name = name
		self.key = key
		self.previousAngle = 0
		self.currentTrack = None
		self.pieceIndex = -1
		self.distanceTravelled = 0
		self.speed = 0

	def msg(self, msg_type, data):
		print("sending: {0}".format(json.dumps({"msgType": msg_type, "data": data})))
		self.send(json.dumps({"msgType": msg_type, "data": data}))

	def send(self, msg):
		self.socket.sendall(msg + "\n")

	def join(self, track):
		if track == None:
			print"Autojoin"
			return self.msg("join", {"name": self.name,
				"key": self.key})
		else:
			r = { 
				"botId": {
					"name": self.name,
					"key": self.key
				},
				"carCount":3
			}
			pprint(r)
			if track.lower() == "any":
				print"joinin any"
			else:
				print"Joining {0}".format(track)
				r["carCount"] = 1
				r["trackName"] = track
				pprint(r)
			
			return self.msg("joinRace", r)

	def throttle(self, throttle):
		self.msg("throttle", throttle)

	def ping(self, debug= None):
		self.msg("ping", {})
		if debug:
			print debug

	def run(self, track):
		self.join(track)
		self.msg_loop()
	
	def on_init(self, data):
		print("Got Init")
		self.currentTrack = Track(data["race"]["track"])
		self.ping()

	def on_join(self, data):
		print("Joined")
		self.ping()

	def on_myCar(self, data):
		print("Got Car")
		self.myCar = data

	def on_game_start(self, data):
		print("Race started")
		self.ping()

	def on_car_positions(self, data):

		for car in data:
			if car["id"]["name"] ==  self.name:
				if (self.pieceIndex != car["piecePosition"]["pieceIndex"]):
					self.pieceIndex = car["piecePosition"]["pieceIndex"]
				if self.previousAngle < abs(car["angle"]):
					self.isBraking = 1;
					self.throttle(0.5)
				else: 
					self.isBraking = 0;
					self.throttle (0.8)
				self.previousAngle = abs(car["angle"])

	def on_crash(self, data):
		print("Someone crashed")
		self.ping()

	def on_game_end(self, data):
		print("Race ended")
		self.ping()

	def on_error(self, data):
		print("Error: {0}".format(data))
		self.ping()

	def msg_loop(self):
		msg_map = {
			'yourcar': self.myCar,
			'gameinit': self.on_init,
			'join': self.on_join,
			'gameStart': self.on_game_start,
			'carPositions': self.on_car_positions,
			'crash': self.on_crash,
			'gameEnd': self.on_game_end,
			'error': self.on_error,
		}
		socket_file = s.makefile()
		line = socket_file.readline()
		while line:
			msg = json.loads(line)
			msg_type, data = msg['msgType'], msg['data']
			if msg_type in msg_map:
				msg_map[msg_type](data)
			else:
				print("Got {0}".format(msg_type))
				self.ping()
			line = socket_file.readline()


if __name__ == "__main__":
	port = 8091
	key = "bLs2TtOMi6cMsQ"
	host = "testserver.helloworldopen.com"
	name = "Athan"
	track = None
	
	print("got message {0}".format(sys.argv))
	
	if len(sys.argv) == 2:
		track = sys.argv[1]
	if len(sys.argv) == 5:
		host, port, name, key = sys.argv[1:5]
	if len(sys.argv) == 6:
		host, port, name, key, track = sys.argv[1:6]

	print("Connecting with parameters:")
	print("host={0}, port={1}, bot name={2}, key={3}".format(host, port, name, key))
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect((host, int(port)))
	bot = NoobBot(s, name, key)
	bot.run(track)
